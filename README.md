# Odhin Theme for [Visual Studio Code](https://code.visualstudio.com/)

> A dark theme for [Visual Studio Code](https://code.visualstudio.com/).

### Base
![Screenshot](https://res.cloudinary.com/odhin/image/upload/v1648371306/projects/odhin-themes/visual-studio-code/screenshot_crrhic.png)

<br>

### High Contrast
![Screenshot High Contrast](https://res.cloudinary.com/odhin/image/upload/v1648371305/projects/odhin-themes/visual-studio-code/screenshot_high_contrast_hvxsyi.png)

<br>

### Soft
![Screenshot Soft](https://res.cloudinary.com/odhin/image/upload/v1648371305/projects/odhin-themes/visual-studio-code/screenshot_soft_i1xjp9.png)

## Install

#### Install using Command Palette

1.  Go to `View -> Command Palette` or press `Ctrl+Shift+P`
2.  Then enter `Install Extension`
3.  Write `Odhin Theme`
4.  Select it or press Enter to install

#### Install using Git

If you are a git user, you can install the theme and keep up to date by cloning the repo:

    git clone https://gitlab.com/odhin-themes/visual-studio-code.git ~/.vscode/extensions/odhin-theme
    cd ~/.vscode/extensions/odhin-theme
    npm install
    npm run build

#### Install using VSIX file

1.  Go to `View -> Extensions` or press `Ctrl+Shift+X`
2.  Then click on `...`
3.  Then enter `Install From VSIX...`
4.  Select the file odhin.vsix to install

#### Activating theme

Run Visual Studio Code. The Odhin Syntax Theme will be available from `File -> Preferences -> Color Theme` dropdown menu.

## Color Palette

![Colors](https://res.cloudinary.com/odhin/image/upload/v1648419991/projects/odhin-themes/visual-studio-code/colors_n37mja.png)

## License

[MIT License](./LICENSE)

## Thanks

This theme was inspired by themes [Dracula](https://github.com/dracula/visual-studio-code/tree/764c3b59aaff75c43399adb9814e143edb494be4) and [Nord](https://github.com/arcticicestudio/nord-visual-studio-code)

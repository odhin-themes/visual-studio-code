# Changelog

## 1.1.2

- Change the logo

## 1.1.0

- Add High Contrast

## 1.0.0

- First version created

Please feel free to request changes or leave feedback.
